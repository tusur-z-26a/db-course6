/* eslint-disable @typescript-eslint/camelcase */
import Router from 'koa-router';
import { green } from 'colors/safe';
import { db } from './db-client';
import { capitalize } from './utils';

enum Tables {
  Areas = 'Areas',
  Contacts = 'Contacts',
  Customers = 'Customers',
  Employees = 'Employees',
  Locations = 'Locations',
  Orders = 'Orders',
  Payment_Methods = 'Payment_Methods',
  Products = 'Products',
  Categories = 'Categories',
}

const router = new Router();

const execQueryParts = (
  queryParts: string[],
  values?: (string | number | undefined)[],
) => {
  const query = queryParts.join(' ');

  const prefix = '[query]';

  // eslint-disable-next-line no-console
  console.debug(
    `${green(prefix)} "${query}"${
      values
        ? `\n${green('vals'.padStart(prefix.length, ' '))} "${values}"`
        : ''
    }`,
  );

  return db.query(query, values);
};

const execQuery = (query: string, values?: (string | number | undefined)[]) =>
  execQueryParts([query], values);

const requiredError = (keyName: string) => `"${keyName}" is required`;

const safeOrderQuery = (query?: string) =>
  query && ['ASC', 'DESC'].includes(query) ? query : 'ASC';

const getAvailableColumns = (table: Tables) =>
  execQuery(
    `SELECT column_name FROM information_schema.columns WHERE table_name='${table}'`,
  ).then(({ rows }) => rows.map(row => row.column_name));

const checkColumns = (table: Tables, columns: string[]) =>
  getAvailableColumns(table).then(availableColumns => {
    const hasUnknownColumns = columns.some(
      column => availableColumns.indexOf(column) === -1,
    );

    if (hasUnknownColumns) {
      throw Error('Received unknown column');
    }
  });

const preprocessType: Router.IMiddleware = async (ctx, next) => {
  const { type }: { type: string } = ctx.params;

  if (typeof type !== 'string') {
    ctx.throw(400, requiredError('type'));
  }

  const processedType = type
    .split('_')
    .map(word => capitalize(word))
    .join('_') as Tables;

  if (!Object.prototype.hasOwnProperty.call(Tables, processedType)) {
    ctx.throw(400, 'Received unknown type');
  }

  ctx.params.type = processedType;

  await next();
};

router.get('/:type', preprocessType, async ctx => {
  type QueryValue = number | string;

  const { type } = ctx.params;

  const queryParts = [`SELECT * FROM "${type}"`];
  const queryValues: QueryValue[] = [];

  const countQueryParts = [`SELECT count(*) FROM "${type}"`];
  const countQueryValues: QueryValue[] = [];

  const [filter, range, sort] = [
    ctx.query.filter,
    ctx.query.range,
    ctx.query.sort,
  ].map(val => val && JSON.parse(val));

  const filterKeys = Object.keys(filter);

  const hasSort = Array.isArray(sort);
  const hasFilter = !!filterKeys.length;

  if (hasFilter) {
    try {
      await checkColumns(type, filterKeys);
    } catch (e) {
      ctx.throw(400, e.message);
    }

    const getQuery = (values: QueryValue[]) =>
      `WHERE ${Object.entries(filter as { [key: string]: string[] })
        .map(
          ([column, filterValues]) =>
            `${column} IN (${(Array.isArray(filterValues)
              ? filterValues
              : [filterValues]
            ).map(value => `$${values.push(value)}`)})`,
        )
        .join(' and ')}`;

    queryParts.push(getQuery(queryValues));
    countQueryParts.push(getQuery(countQueryValues));
  }

  if (hasSort) {
    const [orderBy, orderDirection] = sort;

    try {
      await checkColumns(type, [orderBy]);
    } catch (e) {
      ctx.throw(400, e.message);
    }

    queryParts.push(`ORDER BY ${orderBy} ${safeOrderQuery(orderDirection)}`);
  }

  if (Array.isArray(range)) {
    queryParts.push(
      `LIMIT $${queryValues.push(
        range[1] - range[0] + 1,
      )} OFFSET $${queryValues.push(range[0])}`,
    );
  }

  const { rows } = await execQuery(queryParts.join(' '), queryValues);
  const {
    rows: [{ count }],
  } = await execQuery(countQueryParts.join(' '), countQueryValues);

  ctx.set('Content-Range', rows.length ? `0-0/${count}` : '0-0/0');

  ctx.body = rows;
});

router.post('/:type', preprocessType, async ctx => {
  const {
    params: { type },
    request: { body },
  } = ctx;

  const keys = Object.keys(body);
  const values: (number | string)[] = Object.values(body);

  try {
    await checkColumns(type, keys);
  } catch (e) {
    ctx.throw(400, e.message);
  }

  const {
    rows: [inserted],
  } = await execQuery(
    `INSERT INTO "${type}"(${keys.join()}) VALUES (${values
      .map((_, i) => `$${i + 1}`)
      .join()}) RETURNING id`,
    values,
  );

  ctx.body = {
    id: inserted.id,
  };
});

router.get('/:type/:id', preprocessType, async ctx => {
  const { type } = ctx.params;

  const {
    rows: [row],
  } = await execQuery(`SELECT * FROM "${type}" WHERE id = $1`, [ctx.params.id]);

  if (!row) {
    ctx.throw(404, 'Record not found');
  }

  ctx.body = row;
});

router.put('/:type/:id', preprocessType, async ctx => {
  const { type } = ctx.params;
  const {
    body: { id, ...body },
  } = ctx.request;

  if (Number.isNaN(id)) {
    ctx.throw(400, requiredError('id'));
  }

  if (typeof body !== 'object' || Array.isArray(body)) {
    ctx.throw(400, requiredError('body'));
  }

  try {
    await checkColumns(type, Object.keys(body));
  } catch (e) {
    ctx.throw(400, 'Unknown key');
  }

  const {
    rows: [foundRecord],
  } = await execQuery(`SELECT * FROM "${type}" WHERE id = $1;`, [id]);

  const placeholders = Object.entries(body).map(([key, value], i) => [
    `${key} = $${i + 1}`,
    value,
  ]);

  const query = `UPDATE "${type}" SET ${placeholders
    .map(value => value[0])
    .join()} WHERE id = $${placeholders.length + 1};`;

  const values = [...placeholders.map(value => value[1]), id];

  await (!foundRecord ? execQuery('') : execQuery(query, values));

  const {
    rows: [updatedRecord],
  } = await execQuery(`SELECT * FROM "${type}" WHERE id = $1`, [id]);

  ctx.body = updatedRecord;
});

router.delete('/:type/:id', preprocessType, async ctx => {
  const id = parseInt(ctx.params.id, 10);
  const { type } = ctx.params;

  if (Number.isNaN(id)) {
    ctx.throw(400, requiredError('id'));
  }

  await execQuery(`DELETE FROM "${type}" WHERE id = ${id}`);

  ctx.body = { id };
});

export { router };
