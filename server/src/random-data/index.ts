import { genInsertCustomer, getRandomCustomers } from './generators/customers';
import {
  getInsertLocation,
  createRandomLocationFabric,
} from './generators/locations';
import { Insert, RandomDataGenerator, Table } from './generators/types';
import { genInsertQuery } from './generators';
import { Area, Category, PaymentMethod } from '../models';
import { getInsertEmployee, getRandomEmployees } from './generators/employees';
import { createRandomContactsFabric } from './generators/contacts';
import {
  createRandomProductsFabric,
  getInsertProducts,
} from './generators/products';
import { createRandomOrdersFabric, getInsertOrder } from './generators/orders';
import {
  createRandomOrderDetailsFabric,
  getInsertOrderDetails,
} from './generators/order-details';

const generateQueries = (
  insertFabric: Insert,
  data: ReturnType<RandomDataGenerator>,
) => data.map(customer => insertFabric(customer));

const categories = [
  {
    id: 1,
    name: 'Пицца',
  },
  {
    id: 2,
    name: 'Напитки',
  },
] as Category[];

const paymentMethods = [
  {
    id: 1,
    name: 'Наличный',
  },
  {
    id: 2,
    name: 'Безналичный',
  },
] as PaymentMethod[];

const areas = [
  {
    id: 1,
    name: 'Кировский',
  },
  {
    id: 2,
    name: 'Советский',
  },
  {
    id: 3,
    name: 'Октябрьский',
  },
  {
    id: 4,
    name: 'Ленинский',
  },
] as Area[];

const employees = getRandomEmployees(5);
const customers = getRandomCustomers(20);
const locations = createRandomLocationFabric(areas)(50);
const contacts = createRandomContactsFabric(locations, employees)(
  employees.length,
);
const products = createRandomProductsFabric(categories)(40);
const orders = createRandomOrdersFabric(
  employees,
  paymentMethods,
  customers,
  locations,
)(20);
const orderDetails = createRandomOrderDetailsFabric(products, orders)(
  orders.length,
);

// eslint-disable-next-line no-console
console.log(
  ([
    generateQueries(genInsertQuery(Table.Areas), areas),
    generateQueries(genInsertQuery(Table.Payment_Method), paymentMethods),
    generateQueries(genInsertQuery(Table.Categories), categories),
    generateQueries(getInsertEmployee, employees),
    generateQueries(genInsertCustomer, customers),
    generateQueries(getInsertLocation, locations),
    generateQueries(genInsertQuery(Table.Contacts), contacts),
    generateQueries(getInsertProducts, products),
    generateQueries(getInsertOrder, orders),
    generateQueries(getInsertOrderDetails, orderDetails),
    Object.values(Table)
      .filter(value => value !== Table.Order_Details)
      .map(
        table =>
          `SELECT setval('"${table}_id_seq"', (SELECT MAX(id) from "${table}"));`,
      ),
  ] as string[][])
    .map(queries => queries.join('\n'))
    .join('\n\n'),
);
