import { phone, random } from 'faker/locale/ru';
import { Contact, Employee, Location } from '../../models';
import { pickRandom } from './index';

export const createRandomContactsFabric = (
  locations: Location[],
  employees: Employee[],
) => (length: number) =>
  Array.from(
    { length },
    (_, i): Contact => ({
      id: i + 1,
      location_id: pickRandom(locations).id,
      employee_id: employees[i].id,
      phone: phone.phoneNumber(),
      apartment: random.number(100),
    }),
  );
