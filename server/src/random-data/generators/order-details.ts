import { Order, Product } from '../../models';
import { genInsertQuery, selectRandomUnique } from './index';
import { Table } from './types';

export const getInsertOrderDetails = genInsertQuery(Table.Order_Details);

export const createRandomOrderDetailsFabric = (
  products: Product[],
  orders: Order[],
) => (length: number) => {
  if (orders.length < length) {
    throw Error('Given length should be less or equal to orders length');
  }

  const productIds = products.map(product => product.id);

  const productsByOrders = orders.map(() =>
    selectRandomUnique(productIds, Math.floor(Math.random() * 5) + 1),
  );

  return productsByOrders
    .map((value, i) =>
      value.map(product => ({
        product_id: product,
        order_id: orders[i].id,
        quantity: Math.floor(Math.random() * 5) + 1,
      })),
    )
    .flat();
};
