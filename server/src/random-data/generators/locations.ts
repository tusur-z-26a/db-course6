import { address } from 'faker/locale/ru';
import { genInsertQuery, pickRandom } from './index';
import { Table } from './types';
import { Area, Location } from '../../models';

export const getInsertLocation = genInsertQuery(Table.Locations);

export const createRandomLocationFabric = (areas: Area[]) => (length: number) =>
  Array.from(
    { length },
    (_, id): Location => ({
      id: id + 1,
      street: address.streetName(),
      house_number: Math.floor(Math.random() * 200) + 1,
      area_id: pickRandom(areas).id,
    }),
  );
