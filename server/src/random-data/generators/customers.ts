import { name } from 'faker/locale/ru';
import { Customer } from '../../models';
import { genInsertQuery } from './index';
import { Insert, RandomDataGenerator, Table } from './types';

export const genInsertCustomer = genInsertQuery(Table.Customers);

export const getRandomCustomers = (length: number) =>
  Array.from(
    { length },
    (_, id): Customer => ({
      id: id + 1,
      name: name.findName(),
      has_card: Math.random() > 0.5,
    }),
  );
