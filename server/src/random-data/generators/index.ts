import { identity } from 'ramda';
import { InsertFabric } from './types';
import { Value } from '../../models';

const jsToPgValueMap: {
  [key: string]: Function;
} = {
  boolean: (value: boolean) => String(value),
  number: identity,
  string: (value: string) => `'${value}'`,
  undefined: () => null,
};

const convertToPgData = (value: Value) =>
  jsToPgValueMap[typeof value] ? jsToPgValueMap[typeof value](value) : value;

export const randomDate = (start: Date, end: Date) =>
  new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));

export const pgFormatDate = (date: Date) => {
  const zeroPad = (n: number) => n.toString().padStart(2, '0');

  return `${[
    date.getUTCFullYear(),
    zeroPad(date.getMonth() + 1),
    zeroPad(date.getDate()),
  ].join('-')} ${[
    zeroPad(date.getHours()),
    zeroPad(date.getMinutes()),
    zeroPad(date.getSeconds()),
  ].join(':')}`;
};

export const randomPgDate = (start: Date, end: Date) =>
  pgFormatDate(randomDate(start, end));

const shuffle = (array: any[]) => {
  const lArray = [...array];
  let counter = lArray.length;

  while (counter > 0) {
    const index = Math.floor(Math.random() * counter);

    counter -= 1;

    const temp = lArray[counter];

    lArray[counter] = lArray[index];
    lArray[index] = temp;
  }

  return lArray;
};

export const selectRandom = <T>(array: T[], count: number): T[] =>
  Array.from(
    { length: count },
    () => array[Math.floor(Math.random() * array.length)],
  );

export const selectRandomUnique = <T>(array: T[], count: number): T[] =>
  shuffle(array).slice(0, count);

export const pickRandom = <T>(array: T[]): T => selectRandom(array, 1)[0];

export const genInsertQuery: InsertFabric = table => entries => {
  const joined = Object.entries(entries).reduce(
    (accumulator, entry) => {
      const value = convertToPgData(entry[1]);

      accumulator.columns = `${accumulator.columns}, ${entry[0]}`;
      accumulator.values = `${accumulator.values}, ${value}`;

      return accumulator;
    },
    { columns: '', values: '' },
  );
  const columns = joined.columns.slice(2);
  const values = joined.values.slice(2);

  return `INSERT INTO "${table}"(${columns}) VALUES (${values});`;
};
