import { lorem } from 'faker/locale/ru';
import {
  Customer,
  Employee,
  Location,
  Order,
  PaymentMethod,
} from '../../models';
import { genInsertQuery, pgFormatDate, pickRandom, randomDate } from './index';
import { Table } from './types';

export const getInsertOrder = genInsertQuery(Table.Orders);

export const createRandomOrdersFabric = (
  employees: Employee[],
  paymentMethods: PaymentMethod[],
  customers: Customer[],
  locations: Location[],
) => (length: number) =>
  Array.from(
    { length },
    (_, i): Order => {
      const dateMax = new Date();
      const dateFrom = new Date();
      dateFrom.setMonth(dateMax.getMonth() - 12);
      const orderDate = randomDate(dateFrom, dateMax);
      const deliveryDate = randomDate(orderDate, dateMax);
      const endDate = randomDate(dateFrom, deliveryDate);

      return {
        id: i + 1,
        payment_method_id: pickRandom(paymentMethods).id,
        employee_id: pickRandom(employees).id,
        customer_id: pickRandom(customers).id,
        location_id: pickRandom(locations).id,
        comment: Math.random() > 0.5 ? lorem.paragraph(1) : '',
        delivery_date: pgFormatDate(deliveryDate),
        order_date: pgFormatDate(orderDate),
        end_date: pgFormatDate(endDate),
      };
    },
  );
