import { lorem } from 'faker';
import { Category, Product } from '../../models';
import { genInsertQuery, pickRandom } from './index';
import { Table } from './types';

export const getInsertProducts = genInsertQuery(Table.Products);

export const createRandomProductsFabric = (categories: Category[]) => (
  length: number,
) => {
  return Array.from(
    { length },
    (_, i): Product => {
      const id = i + 1;
      const category = pickRandom(categories);

      return {
        id,
        category_id: category.id,
        name: lorem.words(2),
        price: Math.floor(Math.random() * 30) * 100,
        vegetarian: Math.random() > 0.5,
      };
    },
  );
};
