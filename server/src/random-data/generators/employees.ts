import { name } from 'faker/locale/ru';
import { genInsertQuery, randomPgDate, pickRandom } from './index';
import { Table } from './types';
import { Employee } from '../../models';

const getRandomStartDate = () => randomPgDate(new Date(2017, 1, 1), new Date());

export const getInsertEmployee = genInsertQuery(Table.Employees);

export const getRandomEmployees = (length: number) => {
  const chiefs = Array.from(
    { length: Math.floor(length / 2) },
    () => Math.floor(Math.random() * length) + 1,
  );

  return Array.from(
    { length },
    (_, i): Employee => ({
      id: i + 1,
      name: name.findName(),
      chief_id: Math.random() > 0.5 ? pickRandom(chiefs) : undefined,
      start_date: getRandomStartDate(),
    }),
  ).sort((a, b) => (a.chief_id || 0) - b.id);
};
