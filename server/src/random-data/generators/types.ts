import { ColumnName, Entity, Value } from '../../models';

export enum Table {
  Areas = 'Areas',
  Categories = 'Categories',
  Contacts = 'Contacts',
  Customers = 'Customers',
  Employees = 'Employees',
  Locations = 'Locations',
  Orders = 'Orders',
  Order_Details = 'Order_Details',
  Payment_Method = 'Payment_Method',
  Products = 'Products',
}

export interface Insert {
  // TODO: Fix signature
  (entries: Entity): string;
}

export interface InsertFabric {
  (tableName: Table): Insert;
}

export interface RandomDataGenerator {
  <T extends Entity>(length: number): T[];
}
