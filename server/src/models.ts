export type TableName = string;
export type ColumnName = string;
export type Value = string | number | boolean | undefined;

export type Entity = Record<string, Value>;

export interface Customer extends Entity {
  id: number;
  name: string;
  has_card: boolean;
}

export interface Employee extends Entity {
  id: number;
  name: string;
  start_date: string;
  chief_id?: number;
}

export interface Area extends Entity {
  id: number;
  name: string;
}

export interface Contact extends Entity {
  id: number;
  employee_id: number;
  phone: string;
  apartment: number;
}

export interface Category extends Entity {
  id: number;
  name: string;
}

export interface PaymentMethod extends Entity {
  id: number;
  name: string;
}

export interface Location extends Entity {
  id: number;
  street: string;
  house_number: number;
  area_id: number;
}

export interface Product extends Entity {
  id: number;
  category_id: number;
  name: string;
  price: number;
  vegetarian: boolean;
}

export interface Order extends Entity {
  id: number;
  payment_method_id: number;
  employee_id: number;
  customer_id: number;
  location_id: number;
  comment: string;
  delivery_date: string;
  order_date: string;
  end_date: string;
}

export interface OrderDetails extends Entity {
  product_id: number;
  order_id: number;
  quantity: number;
}
