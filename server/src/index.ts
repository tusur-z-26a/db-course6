import cors from '@koa/cors';
import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import { router } from './routes';

export const app = new Koa();

const port = process.env.PORT || 4000;

app.use(bodyParser());

app.on('error', err => {
  // eslint-disable-next-line no-console
  console.error('server error', err);
});

app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    ctx.body = {
      error: err.message,
    };
    ctx.app.emit('error', err, ctx);
  }
});

app.use(async (ctx, next) => {
  const start = Date.now();

  await next();

  const rt = ctx.response.get('X-Response-Time');
  const responseTime = Date.now() - start;

  ctx.set('X-Response-Time', `${responseTime}ms`);
  // eslint-disable-next-line no-console
  console.log(`${ctx.method} ${ctx.url} - ${rt}`);
});

app.use(
  cors({
    allowMethods: 'GET, PUT, POST, DELETE, OPTIONS',
    origin: '*',
    exposeHeaders: 'Content-Range',
  }),
);

app.use(router.routes());

app.listen(port);

// eslint-disable-next-line no-console
console.info(`Listening to http://localhost:${port} 🚀`);
