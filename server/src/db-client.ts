import { Pool } from 'pg';

const { USER, PASSWORD, DATABASE, HOST, PORT } = process.env;

const requiredFields = {
  user: USER,
  password: PASSWORD,
  database: DATABASE,
};

const optionalFields = {
  host: HOST || 'localhost',
  port: PORT ? parseInt(PORT, 10) : 5432,
};

Object.entries(requiredFields).forEach(([key, value]) => {
  if (typeof value !== 'string') {
    throw Error(
      `Required environment variable ${key} is incorrect (found ${typeof value}, expected string)`,
    );
  }
});

export const db = new Pool({
  ...requiredFields,
  ...optionalFields,
  connectionTimeoutMillis: 30000,
});
