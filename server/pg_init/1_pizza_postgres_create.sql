CREATE TABLE "Products"
(
    "id"          serial   NOT NULL,
    "category_id" smallint NOT NULL,
    "name"        varchar  NOT NULL,
    "price"       int      NOT NULL,
    "vegetarian"  BOOLEAN  NOT NULL,
    CONSTRAINT Products_pk PRIMARY KEY ("id")
);



CREATE TABLE "Categories"
(
    "id"   serial  NOT NULL,
    "name" varchar NOT NULL,
    CONSTRAINT Categories_pk PRIMARY KEY ("id")
);



CREATE TABLE "Order_Details"
(
    "product_id" smallint NOT NULL,
    "order_id"   smallint NOT NULL,
    "quantity"   int      NOT NULL,
    CONSTRAINT Order_Details_pk PRIMARY KEY ("product_id", "order_id")
);



CREATE TABLE "Orders"
(
    "id"                serial   NOT NULL,
    "payment_method_id" smallint NOT NULL,
    "employee_id"       smallint NOT NULL,
    "customer_id"       smallint NOT NULL,
    "location_id"       smallint NOT NULL,
    "comment"           varchar  NOT NULL,
    "delivery_date"     DATE     NOT NULL,
    "order_date"        DATE     NOT NULL,
    "end_date"          DATE     NOT NULL,
    CONSTRAINT Orders_pk PRIMARY KEY ("id")
);



CREATE TABLE "Payment_Method"
(
    "id"   serial  NOT NULL,
    "name" varchar NOT NULL,
    CONSTRAINT Payment_Method_pk PRIMARY KEY ("id")
);



CREATE TABLE "Employees"
(
    "id"         serial  NOT NULL,
    "name"       varchar NOT NULL,
    "chief_id"   smallint,
    "start_date" DATE    NOT NULL,
    CONSTRAINT Employees_pk PRIMARY KEY ("id")
);



CREATE TABLE "Customers"
(
    "id"       serial  NOT NULL,
    "name"     varchar NOT NULL,
    "has_card" BOOLEAN NOT NULL,
    CONSTRAINT Customers_pk PRIMARY KEY ("id")
);



CREATE TABLE "Locations"
(
    "id"           serial   NOT NULL,
    "street"       varchar  NOT NULL,
    "house_number" int      NOT NULL,
    "area_id"      smallint NOT NULL,
    CONSTRAINT Locations_pk PRIMARY KEY ("id")
);



CREATE TABLE "Areas"
(
    "id"   serial  NOT NULL,
    "name" varchar NOT NULL,
    CONSTRAINT Areas_pk PRIMARY KEY ("id")
);



CREATE TABLE "Contacts"
(
    "id"          serial  NOT NULL,
    "location_id" serial  NOT NULL,
    "employee_id" serial  NOT NULL,
    "phone"       varchar NOT NULL,
    "apartment"   int     NOT NULL,
    CONSTRAINT Contacts_pk PRIMARY KEY ("id")
);



ALTER TABLE "Products"
    ADD CONSTRAINT "Products_fk0" FOREIGN KEY ("category_id") REFERENCES "Categories" ("id");


ALTER TABLE "Order_Details"
    ADD CONSTRAINT "Order_Details_fk0" FOREIGN KEY ("product_id") REFERENCES "Products" ("id");
ALTER TABLE "Order_Details"
    ADD CONSTRAINT "Order_Details_fk1" FOREIGN KEY ("order_id") REFERENCES "Orders" ("id");

ALTER TABLE "Orders"
    ADD CONSTRAINT "Orders_fk0" FOREIGN KEY ("payment_method_id") REFERENCES "Payment_Method" ("id");
ALTER TABLE "Orders"
    ADD CONSTRAINT "Orders_fk1" FOREIGN KEY ("employee_id") REFERENCES "Employees" ("id");
ALTER TABLE "Orders"
    ADD CONSTRAINT "Orders_fk2" FOREIGN KEY ("customer_id") REFERENCES "Customers" ("id");
ALTER TABLE "Orders"
    ADD CONSTRAINT "Orders_fk3" FOREIGN KEY ("location_id") REFERENCES "Locations" ("id");


ALTER TABLE "Employees"
    ADD CONSTRAINT "Employees_fk0" FOREIGN KEY ("chief_id") REFERENCES "Employees" ("id");


ALTER TABLE "Locations"
    ADD CONSTRAINT "Locations_fk0" FOREIGN KEY ("area_id") REFERENCES "Areas" ("id");


ALTER TABLE "Contacts"
    ADD CONSTRAINT "Contacts_fk0" FOREIGN KEY ("location_id") REFERENCES "Locations" ("id");
ALTER TABLE "Contacts"
    ADD CONSTRAINT "Contacts_fk1" FOREIGN KEY ("employee_id") REFERENCES "Employees" ("id");
