import React from 'react';
import {
  Edit,
  SimpleForm,
  TextInput,
  ReferenceInput,
  SelectInput,
} from 'react-admin';

export const EmployeeEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="name" />
      <ReferenceInput source="chief_id" reference="employees">
        <SelectInput optionText="name" />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
);
