import React from 'react';
import {
  Edit,
  SimpleForm,
  TextInput,
  ReferenceInput,
  SelectInput,
  NumberInput,
  BooleanInput,
} from 'react-admin';

export const ProductEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <ReferenceInput source="category_id" reference="categories">
        <SelectInput optionText="name" />
      </ReferenceInput>
      <TextInput source="name" />
      <NumberInput source="price" />
      <BooleanInput source="vegetarian" />
    </SimpleForm>
  </Edit>
);
