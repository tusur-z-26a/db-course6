import React from 'react';
import {
  Edit,
  SimpleForm,
  RichTextField,
  ReferenceInput,
  SelectInput,
  DateInput,
} from 'react-admin';

export const OrderEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <ReferenceInput source="payment_method_id" reference="payment_methods">
        <SelectInput optionText="name" />
      </ReferenceInput>
      <ReferenceInput source="employee_id" reference="employees">
        <SelectInput optionText="name" />
      </ReferenceInput>
      <ReferenceInput source="customer_id" reference="customers">
        <SelectInput optionText="name" />
      </ReferenceInput>
      <ReferenceInput source="location_id" reference="locations">
        <SelectInput optionText="name" />
      </ReferenceInput>
      <RichTextField source="comment" />
      <DateInput source="delivery_date" />
      <DateInput source="order_date" />
      <DateInput source="end_date" />
    </SimpleForm>
  </Edit>
);
