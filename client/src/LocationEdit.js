import React from 'react';
import {
  Edit,
  SimpleForm,
  TextInput,
  NumberInput,
  ReferenceInput,
  SelectInput,
} from 'react-admin';

export const LocationEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="street" />
      <NumberInput source="house_number" />
      <ReferenceInput source="area_id" reference="areas">
        <SelectInput optionText="name" />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
);
