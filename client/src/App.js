import CategoryIcon from '@material-ui/icons/CategoryOutlined';
import CreditCardIcon from '@material-ui/icons/CreditCardOutlined';
import DomainIcon from '@material-ui/icons/DomainOutlined';
import LocalOfferIcon from '@material-ui/icons/LocalOfferOutlined';
import LocationCityIcon from '@material-ui/icons/LocationCityOutlined';
import PeopleIcon from '@material-ui/icons/PeopleOutlined';
import SentimentSatisfiedIcon from '@material-ui/icons/SentimentSatisfiedOutlined';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCartOutlined';
import { Admin, EditGuesser, ListGuesser, Resource } from 'react-admin';
import simpleRestProvider from 'ra-data-simple-rest';
import russianMessages from 'ra-language-russian';
import React from 'react';
import { CategoryList } from './CategoryList';
import { OrdersList } from './OrderList';
import { ProductList } from './ProductList';
import { ProductEdit } from './ProductEdit';
import './App.css';
import { CreateProduct } from './CreateProduct';
import { EmployeeList } from './EmployeeList';
import { EmployeeEdit } from './EmployeeEdit';
import { AreaList } from './AreaList';
import { AreaEdit } from './AreaEdit';
import { LocationList } from './LocationList';
import { LocationEdit } from './LocationEdit';

const messages = {
  ru: {
    ...russianMessages,
    resources: {
      products: {
        name: 'Продукт |||| Продукты',
        fields: {
          name: 'Имя',
          category_id: 'Категория',
          vegetarian: 'Вегетарианский',
          price: 'Цена',
        },
      },
      categories: {
        name: 'Категория |||| Категории',
        fields: {
          name: 'Имя',
        },
      },
      customers: {
        name: 'Покупатель |||| Покупатели',
        fields: {
          name: 'Имя',
          has_card: 'Карта',
        },
      },
      employees: {
        name: 'Сотрудник |||| Сотрудники',
        fields: {
          name: 'Имя',
          chief_id: 'Начальник',
          start_date: 'Начало работы',
        },
      },
      areas: {
        name: 'Район |||| Районы',
        fields: {
          name: 'Имя',
        },
      },
      locations: {
        name: 'Адрес |||| Адреса',
        fields: {
          street: 'Улица',
          house_number: 'Номер дома',
          area_id: 'Район',
        },
      },
      orders: {
        name: 'Заказ |||| Заказы',
        fields: {
          payment_method_id: 'Способ оплаты',
          employee_id: 'Сотрудник',
          customer_id: 'Клиент',
          location_id: 'Адрес',
          comment: 'Комментарий',
          delivery_date: 'Дата доставки',
          order_date: 'Дата заказа',
          end_date: 'Дата окончания заказа',
        },
      },
      payment_methods: {
        name: 'Способ оплаты |||| Способы оплаты',
        fields: {
          name: 'Имя',
        },
      },
    },
  },
};

const i18nProvider = locale => messages[locale];

export function App() {
  return (
    <Admin
      dataProvider={simpleRestProvider('http://localhost:4000')}
      locale="ru"
      i18nProvider={i18nProvider}
    >
      <Resource
        name="products"
        icon={ShoppingCartIcon}
        list={ProductList}
        create={CreateProduct}
        edit={ProductEdit}
      />
      <Resource
        name="categories"
        icon={CategoryIcon}
        list={CategoryList}
        edit={EditGuesser}
      />
      <Resource
        name="employees"
        icon={PeopleIcon}
        list={EmployeeList}
        edit={EmployeeEdit}
      />
      <Resource
        name="customers"
        icon={SentimentSatisfiedIcon}
        list={ListGuesser}
        edit={EditGuesser}
      />
      <Resource
        name="areas"
        icon={LocationCityIcon}
        list={AreaList}
        edit={AreaEdit}
      />
      <Resource
        name="locations"
        icon={DomainIcon}
        list={LocationList}
        edit={LocationEdit}
      />
      <Resource
        name="orders"
        icon={LocalOfferIcon}
        list={OrdersList}
        edit={EditGuesser}
      />
      <Resource
        name="payment_methods"
        icon={CreditCardIcon}
        list={ListGuesser}
        edit={EditGuesser}
      />
    </Admin>
  );
}
