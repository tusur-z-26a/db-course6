import React from 'react';
import {
  List,
  Datagrid,
  TextField,
  DateField,
  ReferenceField,
} from 'react-admin';

export const EmployeeList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField source="name" />
      <ReferenceField source="chief_id" reference="employees" allowEmpty>
        <TextField source="name" />
      </ReferenceField>
      <DateField source="start_date" />
    </Datagrid>
  </List>
);
