import React from 'react';
import {
  Filter,
  List,
  Datagrid,
  TextField,
  TextInput,
  ReferenceField,
  ReferenceInput,
  NumberField,
  BooleanField,
  SelectInput,
} from 'react-admin';

const PostFilter = props => (
  <Filter {...props}>
    <TextInput source="name" />
    <ReferenceInput source="category_id" reference="categories">
      <SelectInput source="id" />
    </ReferenceInput>
  </Filter>
);

export const ProductList = props => (
  <List {...props} filters={<PostFilter />}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <ReferenceField source="category_id" reference="categories">
        <TextField source="name" />
      </ReferenceField>
      <TextField source="name" />
      <NumberField source="price" />
      <BooleanField source="vegetarian" />
    </Datagrid>
  </List>
);
