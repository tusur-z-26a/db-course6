import React from 'react';
import {
  List,
  Datagrid,
  TextField,
  NumberField,
  ReferenceField,
} from 'react-admin';

export const LocationList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField source="street" />
      <NumberField source="house_number" />
      <ReferenceField source="area_id" reference="areas">
        <TextField source="name" />
      </ReferenceField>
    </Datagrid>
  </List>
);
