import React from 'react';
import {
  Create,
  SimpleForm,
  TextInput,
  NumberInput,
  BooleanInput,
  SelectInput,
  ReferenceInput,
} from 'react-admin';

const defaultValue = {
  vegetarian: false,
};

export function CreateProduct(props) {
  return (
    <Create {...props}>
      <SimpleForm defaultValue={defaultValue}>
        <ReferenceInput source="category_id" reference="categories">
          <SelectInput optionText="name" />
        </ReferenceInput>
        <TextInput source="name" />
        <NumberInput source="price" />
        <BooleanInput source="vegetarian" />
      </SimpleForm>
    </Create>
  );
}
