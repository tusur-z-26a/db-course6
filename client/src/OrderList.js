import React from 'react';
import {
  List,
  Datagrid,
  FunctionField,
  TextField,
  ReferenceField,
  DateField,
} from 'react-admin';

export const OrdersList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <ReferenceField source="payment_method_id" reference="payment_methods">
        <TextField source="name" />
      </ReferenceField>
      <ReferenceField source="employee_id" reference="employees">
        <TextField source="name" />
      </ReferenceField>
      <ReferenceField source="customer_id" reference="customers">
        <TextField source="name" />
      </ReferenceField>
      <ReferenceField source="location_id" reference="locations">
        <FunctionField
          render={location => `${location.street}, ${location.house_number}`}
        />
      </ReferenceField>
      <DateField source="delivery_date" />
      <DateField source="order_date" />
      <DateField source="end_date" />
    </Datagrid>
  </List>
);
